# Progetto di reti - Traccia 2 - Nikolas Guillen Leon
## Matricola 0000874725

È stato realizzata una versione alternativa del sito di Sky Sport, mantenendo una certa coerenza con lo stile ed i colori del sito originale.
Il server è stato realizzato su localhost (127.0.0.1).
Il sito inizialmente rimanda alla pagina index.html, dalla quale poi ci si può muovere verso i vari articoli del sito e alla form di contatto dalla scritta "Contattaci" nel footer di ogni pagina.


## Html e CSS

Per la base del sito è stato scaricato un template dal web e opportunamente modificato per le esigenze di un sito simil-informativo come può essere il sito di Sky.
Nella home sono presenti le foto e le anteprime del contenuto degli articoli, ai quali è possibile accedere cliccando sul titolo o sul tasto "Leggi di più".

Nel footer del sito è presente, oltre ai collegamenti ai social ufficiali di Sky, anche il tasto "Contattaci", che rimanda ad una form di contatto funzionante.

## Web server

La classe MyServer estende SimpleHTTPRequest, la classe utilizzata anche nel server del Lab 5 per gestire le richieste. Mentre il metodo GET rimane invariato rispetto alla classe originale, il metodo POST permette di salvare su file l'email inserita nella form di contatto.
Infine la funzione send_email invia una mail predefinita tramite una serie di funzioni di libreria di SMTP.
Il loop del server è stato preso dal web server del laboratorio 5.