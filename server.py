''' Traccia 2 - Progetto di programmazione di reti - Nikolas Guillen Leon'''

from http.server import SimpleHTTPRequestHandler
import sys,signal
import socketserver
import smtplib
import cgi

#define port
port = 8080

#define host
host = 'localhost'

#define sender
sender = 'traccia2reti@gmail.com'

#define password
password = 'traccia2'

#define fuction to send email
def send_email():
    
    #read email of receiver
    receiver = open("Email/email.txt","r").read()
    
    message = "Grazie, abbiamo ricevuto correttamente i tuoi dati. Sarai ricontattato quanto prima!"
        
    try:
           serverEmail = smtplib.SMTP('smtp.gmail.com', 587)
           
           #connection with server
           serverEmail.ehlo()
           print('Connected...')
           
           #encrypted channel
           serverEmail.starttls()
           print('Encrypted...')
           
           serverEmail.login(sender, password)
           print('Logged in...')
           
           #send email
           serverEmail.sendmail(sender, receiver, message)
           print('Email sent...')
           
           #close connection
           serverEmail.close()
           print('Connection closed...')
                 
           
           print("Email inviata correttamente")
           
    except Exception:
           print ("Errore: dati mancanti o errati!")
    


#MyServer is the class used to handle HTTP request
#MyServer extends SimpleHTTPRequestHandler
class MyServer(SimpleHTTPRequestHandler):

    print('Server in esecuzione')

    def do_GET(self):
        
        SimpleHTTPRequestHandler.do_GET(self)
            
    def do_POST(self):
        
        # use FieldStorage class to get at submitted from data
        form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE':self.headers['Content-Type'],
                         })
        
        SimpleHTTPRequestHandler.do_GET(self)
        
        # write the last email in txt file
        with open("Email/email.txt", "w") as file:
                for key in form.keys(): 
                    file.write(str(form.getvalue(str(key))))
                    
        #call fuction to send email          
        send_email()
        


server = socketserver.ThreadingTCPServer((host, port), MyServer)

#Allow to use Ctrl-C to terminate every generated thread
server.daemon_threads = True
# Allow to reuse socket even if the last has not been issued
server.allow_reuse_address = True

#fuction to exit from process
def signal_handler(signal, frame):
    print( 'Exiting http server (Ctrl+C pressed)')
    try:
      if( server ):
        server.server_close()
    finally:
      sys.exit(0)

#Interrupt the execution if you type Ctrl-C
signal.signal(signal.SIGINT, signal_handler)

#loop
try:
    
    while True:
        server.serve_forever()

except KeyboardInterrupt:  
    pass

server.server_close()
        

        
